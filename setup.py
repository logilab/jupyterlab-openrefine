import setuptools

setuptools.setup(
    name="jupyter-openrefine-server",
    packages=setuptools.find_packages(),
    package_data={
        'jupyter_openrefine_server': ['icons/*'],
    },
    entry_points={
        'jupyter_serverproxy_servers': [
            'jupyter_openrefine_server = jupyter_openrefine_server:setup_openrefine',
        ]
    },
    install_requires=['jupyter-server-proxy'],
)
