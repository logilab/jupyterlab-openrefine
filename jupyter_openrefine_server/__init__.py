import os


def setup_openrefine():
    return {
        'command': ['refine', '-p', '{port}', '-m', '5120M'],
        'timeout': 40,
        'port': 51947,
        'launcher_entry': {
            'icon_path': os.path.join(
                os.path.dirname(os.path.abspath(__file__)),
                'icons',
                'OpenRefine.svg'
            ),
            'title': "OpenRefine",
        }
    }
